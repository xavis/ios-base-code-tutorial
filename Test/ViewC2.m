//
//  ViewC2.m
//  Test
//
//  Created by Lion User on 22/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewC2.h"
#import "AppDelegate.h"

@interface ViewC2 ()

@end

@implementation ViewC2
@synthesize label;
@synthesize button;
@synthesize prevview;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    button.titleLabel.text = @"Patata";
    button.frame = CGRectMake(0, 0, 100, 100);
    [button addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside]; 
    [self.view addSubview:button];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction)back:(id)sender{
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];

    UIWindow* window = [UIApplication sharedApplication].keyWindow;
    
    [window setRootViewController:[appDelegate.ViewArray objectAtIndex:appDelegate.ViewArray.count-1]];


}

@end
