//
//  Table.m
//  Test
//
//  Created by Lion User on 23/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Table.h"
#import <QuartzCore/QuartzCore.h>

@interface Table()

@end

@implementation Table

@synthesize simpleview;
@synthesize ScrollView;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [ScrollView setContentSize:CGSizeMake(55, 450)];
    [ScrollView setFrame:CGRectMake(0,0,320,55)];
    ScrollView.contentSize = CGSizeMake(450,ScrollView.frame.size.height);
    ScrollView.bounces=false;
    ScrollView.showsHorizontalScrollIndicator=true;
    ScrollView.userInteractionEnabled=true;
    UIButton *subut = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    subut.frame = CGRectMake(12, 12, 430, 30);
    [ScrollView addSubview:subut];


    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSLog(@"Cell is NIL");
        cell = [[UITableViewCell alloc] 
                initWithStyle:UITableViewCellStyleDefault 
                reuseIdentifier:CellIdentifier];
    }    // Configure the cell...
    cell = [[UITableViewCell alloc] 
            initWithStyle:UITableViewCellStyleSubtitle
            reuseIdentifier:CellIdentifier];
    cell.contentView.backgroundColor = [UIColor greenColor];
    cell.textLabel.text = @"Patata";
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.detailTextLabel.text=@"Caliente";
    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
    
    UISwipeGestureRecognizer *Right = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeRight:)];
    [Right setDirection:( UISwipeGestureRecognizerDirectionRight )];
    [cell addGestureRecognizer:Right];

    
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

-(IBAction)handleSwipeRight:(UISwipeGestureRecognizer *)sender{

    UITableViewCell *cell = (UITableViewCell*) sender.view;
    
    UIView *swipeView = [[UIView alloc] initWithFrame:CGRectMake(0,0, cell.bounds.size.width, cell.bounds.size.height)];
    
    UIButton *swipeButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    
    [swipeButton setTitle:cell.textLabel.text forState:UIControlStateNormal];
    [swipeButton addTarget:self action:@selector(swipeButtonClicked:) forControlEvents:UIControlEventTouchUpInside]; 
    
    [swipeView addSubview:swipeButton];
    
    swipeView.backgroundColor =[UIColor grayColor];
    
    
    [cell addSubview:swipeView];
    swipeView.layer.zPosition= 5;
    
    [swipeView.layer setFrame: swipeView.frame];
    
    CABasicAnimation *theAnimation;	
    theAnimation=[CABasicAnimation animationWithKeyPath:@"transform.translation.x"];
    theAnimation.duration=0.2;
    theAnimation.repeatCount=0;
    theAnimation.autoreverses=NO;
    theAnimation.fromValue=[NSNumber numberWithFloat:-320];
    theAnimation.toValue=[NSNumber numberWithFloat:0]; 
   
    [[swipeView layer] addAnimation:theAnimation forKey:@"translateAnimation"];
   
    swipeButton.frame =CGRectMake(20,20,80,60);

}

-(IBAction)swipeButtonClicked:(UIButton *)sender{
  
    CABasicAnimation *theAnimation;	
    theAnimation=[CABasicAnimation animationWithKeyPath:@"transform.translation.x"];
    theAnimation.duration=0.2;
    theAnimation.repeatCount=0;
    theAnimation.autoreverses=NO;
    theAnimation.fromValue=[NSNumber numberWithFloat:0];
    theAnimation.toValue=[NSNumber numberWithFloat:-320]; 
    theAnimation.removedOnCompletion=NO;
    theAnimation.fillMode = kCAFillModeForwards;
    [self removeFromParentViewController];
    [[sender.superview layer] addAnimation:theAnimation forKey:@"translateAnimation"];
    
    
    
}

@end
