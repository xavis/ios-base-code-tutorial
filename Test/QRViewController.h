//
//  QRViewController.h
//  Test
//
//  Created by Lion User on 05/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBarSDK.h"

@interface QRViewController : UIViewController <UIImagePickerControllerDelegate,ZBarReaderDelegate>{
    
    IBOutlet UITextView *resultTextView;
}
@property (nonatomic, retain) IBOutlet UITextView *resultTextView;
@property (nonatomic, retain) UIImagePickerController *imgPicker;

-(IBAction)StartScan:(id) sender;

@end
