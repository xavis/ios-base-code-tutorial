//
//  ViewController.m
//  Test
//
//  Created by Lion User on 19/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"
#import "ViewC2.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"


@interface ViewController ()

@end

@implementation ViewController

@synthesize subview;
@synthesize boton;
@synthesize booot;

- (void)viewDidLoad
{	
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    appDelegate.ViewArray = [[NSArray alloc] init];
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    
    CABasicAnimation *theAnimation;	
    theAnimation=[CABasicAnimation animationWithKeyPath:@"transform.translation.x"];
    theAnimation.duration=3;
    theAnimation.repeatCount=0;
    theAnimation.autoreverses=NO;
    theAnimation.fromValue=[NSNumber numberWithFloat:-650];
    theAnimation.toValue=[NSNumber numberWithFloat:0]; 
    
    CGRect myFrame = CGRectMake(120, 120, 320, 50);
    UIView *myView = [[UIView alloc] initWithFrame:myFrame];
    myView.backgroundColor = [UIColor whiteColor];
    CALayer *newlayer = [[CALayer alloc] init];
    newlayer.backgroundColor = [UIColor blueColor].CGColor;
    newlayer.shadowOffset = CGSizeMake(0, 3);
    newlayer.shadowRadius = 5.0;
    newlayer.shadowColor = [UIColor blackColor].CGColor;
    newlayer.shadowOpacity = 0.8;
    newlayer.frame = CGRectMake(30, 30, 128, 192);
    newlayer.cornerRadius = 10.0; 
    myView.tag =1234;
    [self.view addSubview:myView];
    [myView.layer setShadowOffset:CGSizeMake(0,3)];
    [myView.layer setShadowRadius:10];
    [myView.layer setShadowColor:[UIColor blackColor].CGColor];
    [myView.layer setShadowOpacity:0.8];
    [myView.layer setFrame:CGRectMake(120,120,320,50)];
    [myView.layer setCornerRadius:10];
    UIButton *subut = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    subut.frame = CGRectMake(0, 0, 20, 20);
    subut.titleLabel.text = @"Patata";
    [subut addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside]; 
    [myView addSubview:subut];
    
    
    UISwipeGestureRecognizer *Left = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeLeft:)];
    [Left setDirection:( UISwipeGestureRecognizerDirectionLeft )];
    [self.view addGestureRecognizer:Left];
    
    UISwipeGestureRecognizer *Right = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeRight:)];
    [Right setDirection:(UISwipeGestureRecognizerDirectionRight )];
    [self.view addGestureRecognizer:Right];

    
    UISwipeGestureRecognizer *Up = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeUp:)];
    [Up setDirection:(UISwipeGestureRecognizerDirectionUp )];
    [self.view addGestureRecognizer:Up];
    
    UISwipeGestureRecognizer *Down = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeDown:)];
    [Down setDirection:(UISwipeGestureRecognizerDirectionDown )];
    [self.view addGestureRecognizer:Down];
    
    [super viewDidLoad];
    
    [animation setToValue:[NSNumber numberWithFloat:M_PI * 2.0]];
    [animation setDuration: 3.0];
    
    [[boton layer] addAnimation:animation forKey:@"spinAnimation"];
    
    [[myView layer] addAnimation:theAnimation forKey:@"translateAnimation"];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)delete:(id)sender{
    
    [[self.view.layer.sublayers objectAtIndex:1000] removeFromSuperlayer];
}


-(void)handleSwipeDown:(UISwipeGestureRecognizer *)Down{
    self.view.backgroundColor = [UIColor redColor];}

-(void)handleSwipeUp:(UISwipeGestureRecognizer *)Up{
    self.view.backgroundColor = [UIColor greenColor];}


-(void)handleSwipeRight:(UISwipeGestureRecognizer *)Right{
    self.view.backgroundColor = [UIColor yellowColor];}

-(void)handleSwipeLeft:(UISwipeGestureRecognizer *)Left{
    self.view.backgroundColor = [UIColor blueColor];}

-(IBAction)buttonClicked:(id)sender{
    
    UIWindow* window = [UIApplication sharedApplication].keyWindow;
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    ViewC2 *newview = [[ViewC2 alloc] init];
    newview.view.backgroundColor = [UIColor blackColor];
    NSArray *array = [[NSArray alloc] initWithObjects:self, nil];
    appDelegate.ViewArray = array;
    [UIView
     transitionWithView:window 
     duration:0.5
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^(void) {
         BOOL oldState = [UIView areAnimationsEnabled];
         [UIView setAnimationsEnabled:NO];
         window.rootViewController = newview;
         [UIView setAnimationsEnabled:oldState];
     } 
     completion:nil];
    

}


@end
