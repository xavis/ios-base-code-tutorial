//
//  ViewController.h
//  Test
//
//  Created by Lion User on 19/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIView *subview;
@property (nonatomic, strong) IBOutlet UIButton *boton;
@property (nonatomic, strong) IBOutlet UIButton *booot;


-(void)handleSwipeUp:(UISwipeGestureRecognizer *)Up;

-(void)handleSwipeDown:(UISwipeGestureRecognizer *)Down;

-(void)handleSwipeLeft:(UISwipeGestureRecognizer *)Left;

-(void)handleSwipeRight:(UISwipeGestureRecognizer *)Right;

-(IBAction)delete:(id)sender:(UIView*)view;

-(IBAction)buttonClicked:(UIView*)viewer;

@end
